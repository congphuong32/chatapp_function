const functions = require('firebase-functions');

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
const admin = require('firebase-admin');
admin.initializeApp();

exports.chatHistoryUpdate = functions.database.ref('/chats/{id}/messages/{pushId}')
  .onWrite((change, context) => {
    const { id, pushId } = context.params
    console.log('PUSHID 1: ', id)
    console.log('PUSHID 2: ', pushId)
    const lastMessage = change.after.val().message
    const sender = change.after.val().senderID
    admin.database().ref(`chats/${id}`).child('key').once('value', (snap) => {
      console.log('KEY: ', snap.val())
      let uids = snap.val()
      uids = uids.split('|')
      console.log('UIDS: ', uids)
      if (uids && uids.length > 0) {
        uids.map((uid) => {
          // return admin.database().ref(`/historys/${uid}/${id}`).child('lastMessage').set(lastMessage)
          const timestamp = admin.database.ServerValue.TIMESTAMP
          admin.database().ref(`/history/${uid}/${id}`).set({ key: snap.val(), lastMessage, timestamp })

          if (uid !== senderID) {
            admin.database().ref(`/users/${uid}`).child('token').once('value', (snap) => {
              const token = Object.keys(snap.val())
              const payload = {
                notification: {
                  title: 'Thong bao',
                  body: lastMessage
                }
              }

              admin.messaging().sendToDevice(token, payload).catch((response) => {
                // For each message check if there was an error.
                const tokensToRemove = [];
                response.results.forEach((result, index) => {
                  const error = result.error;
                  if (error) {
                    console.error('Failure sending notification to', tokens[index], error);
                    // Cleanup the tokens who are not registered anymore.
                    if (error.code === 'messaging/invalid-registration-token' ||
                      error.code === 'messaging/registration-token-not-registered') {
                      tokensToRemove.push(tokensSnapshot.ref.child(tokens[index]).remove());
                    }
                  }
                });
                return Promise.all(tokensToRemove);
              });
            })
          }
        })
      }

    })
    console.log('mess', lastMessage)
    // if (uids && uids.length > 0) {
    //   uids.map((uid) => {
    //      return app.database().ref(`/historys/${uid}/${pushId}`).child('lastMessage').set(lastMessage)
    //   })
    // }

    // const deleteApp = () => app.delete().catch(() => null);


    // return deleteApp().then(() => res);
  });


exports.friendRequests = functions.database.ref('/friendRequests/{uid}/{pushId}')
  .onUpdate((change, context) => {
    const val = change.after.val()
    const { uid, pushId } = context.params
    if (val.status && val.status === 1) {
      return Promise.all(admin.database().ref(`/friends/${uid}`).child(`${val.uid}`).set(true),
        admin.database().ref(`/friends/${val.uid}`).child(`${uid}`).set(true))
    }
  });

